<?php

/*
Plugin Name: Generator PDF 
Plugin URI: http://nfinity.pl/
Description: Plugin ułatwiający generowanie PDF na podstawie danych z MailPoet
Author: nFinity
Version: 1.6
Author URI: http://nfinity.pl/
*/

pdf_generator_menu_item();
pdf_generator_submit();

function pdf_generator_menu_item() {
	add_action('admin_menu', 'pdf_generator_main'); 
}

function pdf_generator_main() {
	add_menu_page( 
        'Generator PDF',
        'Generator PDF', 
        'manage_options',
        'generator-PDF',
        'pdf_generator_render'
    );
}

function pdf_generator_render() {
	include('view/pdf_generator_main_view.php');
}

function pdf_generator_submit() {
	add_action( 'admin_post_nfinity_generate_pdf', 'pdf_generator_generate_pdf' );
}

function pdf_generator_generate_pdf() {
	// include('view/pdf_generator_main_generator.php');
	include('view/pdf_generator_class.php');
	exit;
}