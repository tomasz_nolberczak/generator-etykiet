<h1>Generator etykiet i list w PDF</h1>

<?php 

	// Global variable - see here -> https://codex.wordpress.org/Class_Reference/wpdb
	global $wpdb; 

	$all_lists = $wpdb->get_results( 'SELECT * FROM wp_wysija_user_list', OBJECT );

	$uniq_list_of_id = array(); // tutaj pakuję same idki list
	$uniq_list_of_users_id = array(); // tutaj pakuję same idki userów

	// Make an object of all uniq user list
	foreach ($all_lists as $single_list) {

		// Retrieve single list id
		$single_list_id = $single_list->list_id;
		$single_list_id = (int) $single_list_id;

		// Retrieve single user id
		$single_user_id = $single_list->user_id;
		$single_user_id = (int) $single_user_id;


		if (!in_array($single_list_id, $uniq_list_of_id)) {
			array_push($uniq_list_of_id, $single_list_id); // tutaj pakuję same idki list
			array_push($uniq_list_of_users_id, array($single_user_id));
		} else {
			// First, get index of element in array which contain actual single list id
			$counter = 0;

			foreach ($uniq_list_of_id as $item_id) {
				if ($item_id == $single_list_id) {
					break;
				} else {
					$counter++;
				}
			}

			// When found an element - get it's array and add to it next element
			$temp_array = $uniq_list_of_users_id[$counter];
			array_push($temp_array, $single_user_id);
			$uniq_list_of_users_id[$counter] = $temp_array;
			
			// Now we have counter, which is equal to element contain users ;)
			// echo '<div> Element ten należy do listy o id: ' . $uniq_list_of_id[$counter] . '</div>';
		}
	}

	$array_length = count($uniq_list_of_users_id);

?>



<form method="post" action="admin-post.php" target="_blank">
    <input type="hidden" name="action" value="nfinity_generate_pdf" />

    <h4>Ustawienia:</h4>

	<div style="margin-bottom: 12px;">Wielkość etykiety</div>
    <select style="margin-bottom: 20px; width: 189px;" name="size" id="size">
    	<option value="lbl-105x37">105x37</option>
    	<option value="lbl-105x42-4">105x42.4</option>
    	<option value="lbl-105x59-4">105x59.4</option>
    	<option value="by-hand">Ręczne ustawienie</option>
    </select>

    <div style="margin-bottom: 20px;">
    	<label for="borders">
    		<input type="checkbox" name="borders" id="borders" checked="checked">
    		<span>Obramowanie</span>
    	</label>
    </div>

    <div class="manual-table-settings disabled">
    	<h4>Kolumny i wiersze:</h4>
	
		<div style="margin-bottom: 20px;">
	    	<label for="rows">
	    		<div style="margin-bottom: 6px;">Wiersze: </div>
	    		<input type="number" min="0" value="5" name="rows" id="rows">
	    	</label>
	    </div>

	    <div style="margin-bottom: 20px;">
	    	<label for="columns">
	    		<div style="margin-bottom: 6px;">Kolumny: </div>
	    		<input type="number" min="0" value="2" name="columns" id="columns">
	    	</label>
	    </div>
    </div>

    <h4>Marginesy:</h4>

    <div style="margin-bottom: 20px;">
    	<label for="left-margin">
    		<div style="margin-bottom: 6px;">Margines lewy [mm]</div>
    		<input type="number" min="0" value="0" name="left-margin" id="left-margin">
    	</label>
    </div>

    <div style="margin-bottom: 20px;">
    	<label for="right-margin">
    		<div style="margin-bottom: 6px;">Margines prawy [mm]</div>
    		<input type="number" min="0" value="0" name="right-margin" id="right-margin">
    	</label>
    </div>

    <div style="margin-bottom: 20px;">
    	<label for="top-margin">
    		<div style="margin-bottom: 6px;">Margines górny [mm]</div>
    		<input type="number" min="0" value="0" name="top-margin" id="top-margin">
    	</label>
    </div>

    <div style="margin-bottom: 20px;">
    	<label for="bottom-margin">
    		<div style="margin-bottom: 6px;">Margines dolny [mm]</div>
    		<input type="number" min="0" value="0" name="bottom-margin" id="bottom-margin">
    	</label>
    </div>

    <h4>Listy:</h4>

	<table style="overflow: auto; max-height: 400px; width: 600px;">
		<tbody>
			<?php $index = 0 ?>
			<?php foreach ($uniq_list_of_id as $item) { ?>
			<?php $item_from_query = $wpdb->get_results( 'SELECT * FROM wp_wysija_list WHERE list_id = ' . $item, OBJECT ); ?>
				<tr>
					<td>
						<?php $array_of_users_id = null ?>
						<?php $array_of_users_id = implode(",", $uniq_list_of_users_id[$index]) ?>

						<label for="<?php echo $item_from_query[0]->list_id ?>">
							<input type="hidden" name="users_id[]" value="<?php echo $array_of_users_id ?>">
							<input type="checkbox" id="<?php echo $item_from_query[0]->list_id ?>" name="list_id[]" value="<?php echo $index ?>">
							<?php echo $item_from_query[0]->name ?>
							<span>(Użytkowników: <?php echo count($uniq_list_of_users_id[$index]) ?>)</span>
						</label>
					</td>
				</tr>
				<?php $index++ ?>
			<?php } ?>
		</tbody>
	</table>

	<h4>Format generowanych danych:</h4>

	<div class="alert warning">
		Uwaga! Tylko etykiety obsługują marginesy, obramowanie, wielkość etykiety, kolumny oraz wiersze.
	</div>

	<div>
    	<div>
    		<label for="generate-label">
	    		<input type="radio" value="generate-label" name="generate-type" id="generate-label" checked="checked">
	    		<div style="margin-bottom: 6px; display: inline-block; margin-left: 6px;">Generuj etykiety w formacie PDF</div>
	    	</label>
    	</div>
		<div>
			<label for="generate-list">
	    		<input type="radio" value="generate-list" name="generate-type" id="generate-list">
	    		<div style="margin-bottom: 6px; display: inline-block; margin-left: 6px;">Generuj listę w formacie PDF</div>
	    	</label>
		</div>
	</div>

	<div class="clearfix" style="margin-top: 40px;">
		<!-- <a href="#" class="button" style="float: left; margin-right: 10px;">Podejrzyj</a> -->
		<input type="submit" name="submit" id="submit" class="button button-primary" value="Generuj PDF" style="float: left; margin-right: 10px;">
	</div>
</form>	

<style>
	.manual-table-settings {
		opacity: 1;
		-webkit-transition: opacity 300ms ease;
		   -moz-transition: opacity 300ms ease;
		    -ms-transition: opacity 300ms ease;
		     -o-transition: opacity 300ms ease;
		        transition: opacity 300ms ease;
	}

	.manual-table-settings.disabled {
		opacity: 0.2;
		position: relative;
		z-index: 8;
	}

	.manual-table-settings.disabled:after {
		width: 100%;
		height: 100%;
		content: '';
		display: block;
		position: absolute;
		left: 0;
		top: 0;
		z-index: 9;

	}

	.alert.warning {
		padding: 15px;
		margin: 15px 0px;
		background-color: #E0E0E0;
		font-size: 12px;
		max-width: 450px;
		line-height: 19px;
	}
</style>

<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script>
	$(document).ready(function() {
		Form.init();
	});

	var Form = {
		init: function() {
			this.activateManualTable();
		},

		activateManualTable: function() {
			$('#size').change(function(event) {
				var container = $('.manual-table-settings');

				if ($(this).val() == 'by-hand') {
					container.removeClass('disabled');
				} else {
					container.addClass('disabled');
				}
			});
		}
	}
</script>