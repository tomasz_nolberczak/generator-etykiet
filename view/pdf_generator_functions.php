<?php 

// Return array of users ID from wysija plugin database tables
// all_users_id -> tablica list z ajdikami  użytkowników - po kolei
function getUsersID($all_users_id, $chosen_list_id) {

	// Blank variable for storing all users ID of chosen list
	$chosen_users_id = array();

	// Loop for finding chosen users ID
	foreach ($chosen_list_id as $single_list_id) {
		array_push($chosen_users_id, $all_users_id[(int) $single_list_id]);
	}

	// Connect two arrays and make string
	$chosen_users_id = implode(',', $chosen_users_id);
	
	// Move from string type to array
	$chosen_users_id = explode(',', $chosen_users_id);

	// Get only unique elements in array
	$chosen_users_id = array_unique($chosen_users_id);

	// Return array 
	return $chosen_users_id;
}

// Return array of information about chosen users 
function getUsersData($users_id, $wpdb) {
	// $all_users_from_database =
	$users_data = array();

	foreach ($users_id as $item) {
		$single_user_data =  $wpdb->get_results( 'SELECT * FROM wp_wysija_user WHERE user_id = ' . (int)$item, OBJECT );

		// Push to array
		array_push($users_data, $single_user_data);
	}

	return $users_data;
}