<?php

error_reporting(E_ALL); 
ini_set("display_errors", 1); 	

require 'pdf_generator_functions.php';


global $wpdb;

class PDF_Generator {

	const A4_WIDTH = 210;
	const A4_HEIGHT = 297;

	private $options
		    , $current_row
				, $current_col;

	private function reset_current() {
		$this->current_row = 0;
		$this->current_col = 0;
	}

	private function marginLeft() {
		return (float)$this->post('left-margin', 0);
	}

	private function marginRight() {
		return (float)$this->post('right-margin', 0);
	}

	private function marginTop() {
		return (float)$this->post('top-margin', 0);
	}

	private function marginBottom() {
		return (float)$this->post('bottom-margin', 0);
	}

	private function marginWidth() {
		return $this->marginLeft() + $this->marginRight();
	}

	private function marginHeight() {
		return $this->marginTop() + $this->marginRight();
	}

	private function rows() {
		return $this->post('rows', 10);
	}

	private function cols() {
		return $this->post('columns', 2);
	}

	private function incrementRow() {
		if ($this->current_row+1 >= $this->rows()) {
			$this->current_row = 0;
			$this->current_col = 0;
		} else {
			$this->current_row++;
		}
	}

	private function incrementCol() {
		if ($this->current_col+1 >= $this->cols()) {
			$this->current_col = 0;
			$this->incrementRow();
		} else {
			$this->current_col++;
		}
	}

	public function __construct($options) {
		$this->options = $options;
		$this->reset_current();
	}

	public function post($k, $d = null) {
		if (isset($_POST[$k])) {
			return $_POST[$k];
		}
		return $d;
	}

	private function bordersHeight() {
		return round(($this->rows()+1)*0.5, 2);
	}

	private function bordersWidth() {
		return round(($this->cols()+1)*0.5, 2);
	}

	public function wordpressDatabase() {
		global $wpdb;
		$this->wpdb = $wpdb;
		return $this->wpdb;
	}

	public function chosen_list_id() {
		return $this->post('list_id');
	}

	public function all_users_id() {
		return $this->post('users_id');
	}

	public function generate_type() {
		return $this->post('generate-type');
	}

	public function dispatch() {
		$this->size = $this->post('size');

		if ($this->generate_type() == 'generate-label'):
			$this->renderPDFLabel();
		else:
			$this->renderPDFList();
		endif;
	}

	public function renderPDFList() {

		try { 
			$pdf = $this->wkhtmltopdf($this->renderPDFListHeader().$this->renderPDFListBody().$this->renderPDFListFooter());

			//header('Content-Disposition: attachment; filename="wydruk.pdf"');
			header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
			header("Pragma: no-cache"); // HTTP 1.0.
			header("Expires: 0"); // Proxies.
			header('Content-Type: application/pdf');
			echo $pdf;
			
		} catch (Exception $e) {
			echo $e;
			echo "Wystąpił błąd";
		}
	}

	public function renderPDFListHeader() {
		$header = null; 
		$header .= '<!DOCTYPE HTML>';
		$header .= '<html>';
		$header .= '<head>';
		$header .= '<meta http-equiv="content-type" content="text/html; charset=utf-8" />';
		$header .= '<title>Index</title>';
		$header .= '</head>';
		$header .= '<body style="padding: 15mm;">';
		$header .= '<h3>Lista zapisanych użytkowników:</h3>';

		return $header;
	}

	public function renderPDFListBody() {
		// Get array of chosen users ID
		$chosen_users_id = getUsersID($this->all_users_id(), $this->chosen_list_id());

		// Get information about chosen users
		$rows = getUsersData($chosen_users_id, $this->wordpressDatabase());
		
		ob_start();

		foreach ($rows as $row):
			echo $this->renderPDFListItem($row);
		endforeach;

		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	public function renderPDFListItem($row) {

		$first_name  = $row[0]->firstname;
		$last_name   = $row[0]->lastname;
		$institution = $row[0]->cf_1;
		$street      = $row[0]->cf_2;
		$number      = $row[0]->cf_3;
		$postal_code = $row[0]->cf_5;
		$town        = $row[0]->cf_6;
		$function    = $row[0]->cf_4;

		$html = null;
		$html .= '<div style="position:relative; border-bottom: 1px dashed #eee">';
		$html .= '<p>';
		$html .= $first_name . '&nbsp;';
		$html .= $last_name . '&nbsp;';
		$html .= $institution . '&nbsp;';
		$html .= $street . '&nbsp;';
		$html .= $number . '&nbsp;';
		$html .= $postal_code . '&nbsp;';
		$html .= $town . '&nbsp;';
		$html .= $function . '&nbsp;';
		$html .= '</p>';
		// $html .= '<p>';
		// $html .= $street . '&nbsp;';
		// $html .= $number . '&nbsp;';
		// $html .= '</p>';
		// $html .= '<p>';
		// $html .= $postal_code . '&nbsp;';
		// $html .= $town . '&nbsp;';
		// $html .= '</p>';
		$html .= '</div>';

		return $html;
	}

	public function renderPDFListFooter() {
		$html .= '</body>';
		$html .= '</html>';

		return $html;
	}

	public function renderPDFLabel() {


		try { 
			$pdf = $this->wkhtmltopdf($this->renderPDFLabelsHeader().$this->renderPDFLabelsBody().$this->renderPDFLabelsFooter());

			//header('Content-Disposition: attachment; filename="wydruk.pdf"');
			header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
			header("Pragma: no-cache"); // HTTP 1.0.
			header("Expires: 0"); // Proxies.
			header('Content-Type: application/pdf');
			echo $pdf;
			
		} catch (Exception $e) {
			echo $e;
			echo "Wystąpił błąd";
		}
	}

	public function wkhtmltopdf($content) {
		$descriptorspec = array(
			0 => array("pipe", "r"), // stdin
			1 => array("pipe", "w"), // stdout
			2 => array("pipe", "w"), // stderr
		);

		$cmd = $this->options['wkhtmltopdf']
			. ' --margin-left 0mm'//.$this->marginLeft().'mm'
			. ' --margin-right 0mm'//.$this->marginRight().'mm'
			. ' --margin-top 0mm'//.$this->marginTop().'mm'
			. ' --margin-bottom 0mm'//.$this->marginBottom().'mm'
			. ' --disable-smart-shrinking'
			. ' --dpi 300'
			. ' --zoom 0.781'
			. ' - -';

		$proc = proc_open($cmd, $descriptorspec, $pipes);

		if (is_resource($proc))
		{
			// close stdin
			fwrite($pipes[0], $content);
			fclose($pipes[0]);

			// close stdout
			$stdout = stream_get_contents($pipes[1]);
			fclose($pipes[1]);

			// close stderr
			$stderr = stream_get_contents($pipes[2]);
			fclose($pipes[2]);

			$retval = proc_close($proc);

			if ($retval == 0) {
				return $stdout;
			}
		}

		throw new Exception($stderr);
	}


	

	public function renderPDFLabelsHeader($noform = false) {

		switch($this->post('size')) {
			case 'lbl-105x37':
				$_POST['size'] = 'by-hand';
				$_POST['columns'] = 2;
				$_POST['rows'] = 8;
				break;
			case 'lbl-105x42-4':
				$_POST['size'] = 'by-hand';
				$_POST['columns'] = 2;
				$_POST['rows'] = 7;
				break;
			case 'lbl-105x59-4':
				$_POST['size'] = 'by-hand';
				$_POST['columns'] = 2;
				$_POST['rows'] = 5;
				break;
		}

		$border = $noform ? '0px' : '1px dashed #ccc';

		$additional_css = '';
		if ($this->post('size') == 'by-hand') {
			$additional_css = '; width: '.
				round((self::A4_WIDTH - $this->marginWidth() - $this->bordersWidth())/$this->cols(), 2).
				'mm ; height: '.                                                                                         
				round((self::A4_HEIGHT - $this->marginHeight() - $this->bordersHeight()) / $this->rows(), 2).
				'mm';

			$additional_css .= '; max-width: '.
				round((self::A4_WIDTH - $this->marginWidth() - $this->bordersWidth())/$this->cols(), 2).
				'mm ; max-height: '.                                                                                         
				round((self::A4_HEIGHT - $this->marginHeight() - $this->bordersHeight()) / $this->rows(), 2).
				'mm';
		}

		if ($this->post('borders')) {
			$additional_css .= '; border: 0.5mm dashed #000';
		}

		$lwidth = (self::A4_WIDTH - $this->marginWidth())/2;

		$mt = $this->marginTop();
		$mb = $this->marginBottom();
		$ml = $this->marginLeft();
		$mr = $this->marginRight();

		$w = self::A4_WIDTH - $this->marginWidth();
		$h = self::A4_HEIGHT - $this->marginHeight();

		$a4w = self::A4_WIDTH;
		$a4h = self::A4_HEIGHT;
		$a4h2 = $a4h;

		$header = null; 
		$header .= '<!DOCTYPE HTML>';
		$header .= '<html>';
		$header .= '<head>';
		$header .= '<meta http-equiv="content-type" content="text/html; charset=utf-8" />';
		$header .= '<title>Index</title>';
		$header .= '<style type="text/css">';
		$header .= '@font-face {';
		$header .= 'font-family: times;';
		$header .= 'src: url(http://nfinity-beta: .%;l/wozownia/wordpress/rzr-labels/LiberationSerif-Regular.ttf);';
		$header .= '}';
		$header .= '* { font-family: times }';
		$header .= 'body {';
		$header .= 'margin: 0px;';
	  	$header .= 'padding: 0px;';
	  	$header .= 'border: 0px;';
	  	$header .= 'outline: none;';
		$header .= '}';
		$header .= '.a4 { position: absolute; top: 0mm; bottom: 0mm; background: white; margin: 0px auto; padding: 0px; outline: none; border: ' . $border . ' }';
		$header .= '.lbl { ';
		$header .= 'page-break-inside:avoid; overflow: hidden; vertical-align: top; margin: 0px; padding: 0px; outline: none; border: 0px; white-space: nowrap ' . $additional_css;
		$header .= '}';
		$header .= 'div.div-container { width: ' . $a4w . 'mm; height: ' . $a4h2 . 'mm; margin:auto; page-break-inside: avoid }';
		$header .= 'div.tbl-container { width: ' . $a4w . 'mm; height: ' . $a4h . 'mm; margin:auto; vertical-align:middle; display:table-cell; page-break-inside: avoid }';
		$header .= 'div.page-breaker { display: block; height: 0px; clear: both; page-break-after: always }';
		$header .= 'table.lbl-container { border-collapse:collapse;width:100%;page-break-inside:avoid;table-layout:fixed;white-space:nowrap; width: ' . $w . 'mm; height: ' . $h . 'mm; margin:auto; vertical-align:middle }';
		$header .= 'table.lbl-container tr { page-break-inside:avoid }';
		$header .= '.lbl p { padding: 0mm 4mm }';
		$header .= '</style>';
		$header .= '</head>';
		$header .= '<body>';

		return $header;
	}

	public function renderPDFLabelsBody() {
		$this->reset_current();

		// Get array of chosen users ID
		$chosen_users_id = getUsersID($this->all_users_id(), $this->chosen_list_id());

		// Get information about chosen users
		$rows = getUsersData($chosen_users_id, $this->wordpressDatabase());

		ob_start();
?>
		<div class="a4">
			<?php $idx = 0 ?>
			<?php foreach ($rows as $row): ?>
				<?php $idx++; if (!$this->post('skip_header') || $idx > 1): ?>
					<?php if ($this->current_row == 0 && $this->current_col == 0): ?>
						<div class="div-container"<?php if (count($rows) - $idx < $this->rows()): ?> style="padding-top:<?= $this->marginTop() ?>mm; vertical-align:top; height: <?= self::A4_HEIGHT-$this->marginTop() ?>mm"<?php endif ?>>
						<div class="tbl-container"<?php if (count($rows) - $idx < $this->rows()): ?> style="vertical-align:top; height: <?= self::A4_HEIGHT-$this->marginTop() ?>mm"<?php endif ?>>
						<table class="lbl-container"<?php if (count($rows) - $idx < $this->rows()): ?> style="height: <?= (count($rows) - $idx-1)*(self::A4_HEIGHT-$this->marginHeight())/$this->rows() ?>mm"<?php endif ?>>
					<?php endif ?>
					<?php if ($this->current_col == 0): ?>
						<tr>
					<?php endif ?>

					<?php echo $this->renderLabel($row) ?>

					<?php if ($this->current_col+1 >= $this->cols()): ?>
						</tr>
					<?php endif ?>
					<?php if ($this->current_row+1 >= $this->rows() && $this->current_col+1 >= $this->cols()): ?>
						</table>
						</div>
						</div>
						<div class="page-breaker">&nbsp;</div>
					<?php endif ?>

					<?php $this->incrementCol() ?>

				<?php endif ?>
			<?php endforeach ?>
		</div>
	<?php
		$content = ob_get_contents();
		ob_end_clean();

		// fclose($fp);
		$this->reset_current();

		return $content;
	}

	public function renderLabel($row) {

		$first_name  = $row[0]->firstname;
		$last_name   = $row[0]->lastname;
		// $institution = $row[0]->cf_1;
		// $street      = $row[0]->cf_2;
		// $number      = $row[0]->cf_3;
		// $postal_code = $row[0]->cf_5;
		// $town        = $row[0]->cf_6;
		// $function    = $row[0]->cf_4;

		$height = round((self::A4_HEIGHT - $this->marginHeight()) / $this->rows(), 2);
		$html = null;

		$html .= '<td class="lbl ' . $this->size . '">';
		$html .= '<div style="position:relative">';
		$html .= '<p>';
		// $html .= $function . '&nbsp;';
		$html .= $first_name . '&nbsp;';
		$html .= $last_name . '&nbsp;';
		$html .= '</p>';
		// $html .= '<p>';
		// $html .= $street . '&nbsp;';
		// $html .= $number . '&nbsp;';
		// $html .= '</p>';
		// $html .= '<p>';
		// $html .= $postal_code . '&nbsp;';
		// $html .= $town . '&nbsp;';
		// $html .= '</p>';
		$html .= '</div>';
		$html .= '</td>';

		return $html;
	}

	public function renderPDFLabelsFooter() {
		$html = null;
		$html .= '</body>';
		$html .= '</html>';

		return $html;
	}
}

$executable = __DIR__.'/wkhtmltopdf';

if (!is_executable($executable)) {
	$executable = '/usr/bin/wkhtmltopdf';
	if (!is_executable($executable)) {
		$executable = '/usr/local/bin/wkhtmltopdf';
		if (!is_executable($executable)) {
			die('Could not find wkhtmltopdf!');
		}
	}
}

$app = new PDF_Generator(array(
	'wkhtmltopdf' => $executable
));

$app->dispatch();
